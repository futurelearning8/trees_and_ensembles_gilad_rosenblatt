import pandas as pd
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import StandardScaler
from sklearn.tree import DecisionTreeRegressor
from sklearn.metrics import r2_score, mean_squared_error


class Pipeline:
    """
    Pipeline to read > clean > split > normalize > fit > score a scikit-learn model.
    """

    # Column interpretation.
    interpretation_dict = {
        "AT": "Temperature",
        "AP": "Ambient Pressure",
        "RH": "Relative Humidity",
        "V": "Exhaust Vacuum",
        "PE": "Net hourly electrical energy output"
    }

    # Path to data.
    filename = "../data/CCPP/ccpp.xlsx"

    # Target column names.
    target = ["PE"]

    # Feature column names.
    features = [key for key in interpretation_dict.keys() if key != "PE"]

    def __init__(self, score_metric=r2_score, model_factory=DecisionTreeRegressor):
        """Instantiate a new pipeline for the named model."""

        # Factory method for the model to instantiate from.
        self.model_factory = model_factory

        # Score method given y and y_preds to evaluate model performance.
        self._score = score_metric

        # Sklearn model object (will-be after calling fit).
        self.model = None

    @staticmethod
    def read():
        return pd.read_excel(Pipeline.filename)

    @staticmethod
    def clean(df):

        # Drop rows for which any of the features or target is missing and shuffle in place.
        df = df \
            .dropna(how="any") \
            .sample(frac=1, random_state=42)

        # Extract numpy arrays for regression.
        X = df[Pipeline.features].values
        y = df[Pipeline.target].values

        # Return features and targets.
        return X, y

    @staticmethod
    def split(X, y):
        X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.33, random_state=42)
        return X_train, X_test, y_train, y_test

    @staticmethod
    def normalize(X_train, X_test, y_train, y_test):
        scaler = StandardScaler()
        scaler.fit(X_train)
        return scaler.transform(X_train), scaler.transform(X_test), y_train, y_test

    def fit(self, X_train, X_test, y_train, y_test, *args, **kwargs):
        self.model = self.model_factory(*args, **kwargs)
        self.model.fit(X_train, y_train.reshape(-1, ))
        return self.model.predict(X_train), self.model.predict(X_test), y_train, y_test

    def score(self, y_train_preds, y_test_preds, y_train, y_test):
        return self._score(y_train, y_train_preds), self._score(y_test, y_test_preds)

    def run(self, *args, **kwargs):
        """Runs the pipeline read + shuffle > clean + select features > split > normalize > fit > score."""
        train_score, test_score = \
            self.score(
                *self.fit(
                    *Pipeline.normalize(
                        *Pipeline.split(
                            *Pipeline.clean(
                                Pipeline.read()
                            )
                        )
                    ),
                    *args,
                    **kwargs
                )
            )
        return train_score, test_score

    def explain_model(self):
        print(f"------------------ Model Explanation -------------------")
        print(f"Model: {self.model.__class__.__name__}")
        print(f"Targets: {', '.join(Pipeline.target)}.")
        print(f"Features: {', '.join(Pipeline.features)}.")
        print(f"--------------------------------------------------------")

    def report_score(self, y, y_preds):
        self.print_score(self._score(y, y_preds))

    def print_score(self, score, set_name=None, hyperparameter_dict=None):
        """
        Utility method to print scores for this model using (external) inputs.

        :param float score: metric score to print.
        :param str set_name: name of set for which score was obtained (e.g., "train", "validation", "test").
        :param dict hyperparameter_dict: hyperparameter names and values for which score was obtained on this model.
        """
        print(f"--------------------- SCORE ----------------------------")
        print(f"Model: {self.model.__class__.__name__}")
        if hyperparameter_dict:
            for name, value in hyperparameter_dict.items():
                print(f"Hyperparameter: {name} = {value}")
        print(f"Metric: {self._score.__name__}")
        if set_name:
            print(f"Set: {set_name}")
        print(f"Value: {score:.6f}")
        print(f"--------------------------------------------------------")


def main():
    """Run a pipeline to read > clean > split > normalize > fit > score a tree regression model using scikit-learn."""
    pipeline = Pipeline(
        score_metric=mean_squared_error,
        model_factory=DecisionTreeRegressor
    )
    train_score, test_score = pipeline.run()
    pipeline.print_score(train_score, set_name="train")
    pipeline.print_score(test_score, set_name="test")


if __name__ == '__main__':
    main()
