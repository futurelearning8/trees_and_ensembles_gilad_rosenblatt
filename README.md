# Tree Models and Ensembles

This is the tree models and ensembles exercise for FL8 week 2 day 3 by Gilad Rosenblatt.

##Instructions

### Run

Run `main.py` in the terminal from `code` as current working directory.

### Data

Data is not included in this repo. The ```data``` directory should in the repo base directory,
one level up the tree from `code`. The path to the data file is assumed to be `data/CCPP/ccpp.xlsx`. The dataset can be found [here](https://archive.ics.uci.edu/ml/datasets/combined+cycle+power+plant).

## License

[WTFPL](http://www.wtfpl.net/)
